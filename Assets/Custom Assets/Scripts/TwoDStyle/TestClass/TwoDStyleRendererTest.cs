﻿using UnityEngine;
using System.Collections;

public class TwoDStyleRendererTest : MonoBehaviour {

    public TwoDStyle.TwoDStyleRenderer reference;
	// Use this for initialization
	void Start () {
        reference.OnStateChanged += this.aFunction;
    }
	
    public void aFunction(string renderingState)
    {
        Debug.Log(" state is : " + renderingState);
    }
	// Update is called once per frame
	void Update () {
	
	}
}
