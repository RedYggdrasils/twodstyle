﻿using UnityEngine;
using System.Collections;

public class setActiveSelf : MonoBehaviour {

    public float activeSelfTime = 46.0f;
	// Use this for initialization
	void Start () {
        Invoke("setActiveSelfFunction", activeSelfTime);
	}
	
    private void setActiveSelfFunction()
    {
        this.GetComponent<Camera>().enabled = true;
    }
}
